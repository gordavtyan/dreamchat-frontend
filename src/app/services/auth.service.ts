import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})

export class AuthService {

    constructor(
        private router: Router
    ) {
    }

    public get isAuthenticated(): boolean {
        return !!localStorage.getItem('token');
    }

    public logOut(): void {
        localStorage.removeItem('token');
        localStorage.removeItem('user');
        this.router.navigate(['/login']);
    }

    public setToken(token: string): void {
        localStorage.setItem('token', token);
        this.router.navigate(['/']);
    }

    public setUserInfo(data: any): void {
        localStorage.setItem('user', JSON.stringify(data));
    }

    public getUserInfo(): string {
        return localStorage.getItem('user');
    }

    public getToken(): string {
        return localStorage.getItem('token');
    }
}
