import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { empty, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';
import { catchError, map } from 'rxjs/operators';
import { Router } from '@angular/router';

export interface ResponseInterface {
    success: boolean;
    data: any;
}

@Injectable({
    providedIn: 'root'
})
export class HttpService {

    constructor(
        private http: HttpClient,
        private authService: AuthService,
        private router: Router
    ) {
        this.getHeaders();
    }

    public get(
        endpoint: string,
        includeAuthorization: boolean = false,
        headers: { [name: string]: string } = {},
    ): Observable<any> {
        return this.http.get(
            `${environment.apiUrl}${endpoint}`,
            {headers: this.getHeaders(includeAuthorization, headers)}
        ).pipe(
            catchError(error => {
                this.handleError(error);
                return empty();
            })
        );
    }

    public delete(
        endpoint: string,
        includeAuthorization: boolean = false,
        headers: { [name: string]: string } = {}
    ): Observable<any> {
        return this.http.delete(
            `${environment.apiUrl}${endpoint}`,
            {headers: this.getHeaders(includeAuthorization, headers)}
        ).pipe(
            catchError(error => {
                this.handleError(error);
                return empty();
            })
        );
    }

    public post(
        endpoint: string,
        body: Object,
        includeAuthorization: boolean = false,
        headers: { [name: string]: string } = {},
    ): Observable<any> {
        return this.http.post(
            `${environment.apiUrl}${endpoint}`,
            body,
            {headers: this.getHeaders(includeAuthorization, headers)}
        ).pipe(
            catchError(error => {
                this.handleError(error);
                return empty();
            })
        );
    }

    public put(
        endpoint: string,
        body: Object,
        includeAuthorization: boolean = true,
        headers: {[name: string]: string} = {}
    ) {
        return this.http.put(
            `${environment.apiUrl}${endpoint}`,
            body,
            {headers: this.getHeaders(includeAuthorization, headers)}
        ).pipe(
            map((data: ResponseInterface) => {
                if (data.success) {
                    return data.data;
                } else {}
            }),
            catchError(error => {
                this.handleError(error);
                return empty();
            })
        );
    }

    private getHeaders(
        withAuth: boolean = true,
        options: { [name: string]: string } = {},
    ): HttpHeaders {
        const defaults: { [name: string]: string } = {
            'Content-Type': 'application/json'
        };
        withAuth && (defaults['x-access-token'] = `${this.authService.getToken()}`);
        options = Object.assign({}, defaults, options);
        return new HttpHeaders(options);
    }

    private handleError(error: HttpErrorResponse) {
        switch (error.status) {
            case 401:
                this.authService.logOut();
                this.router.navigate(['/login']);
                break;
            case 400:
                break;
            default:
                break;
        }
    }
}
