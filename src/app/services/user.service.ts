import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';

import {HttpService} from './http.service';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(
        private router: Router,
        private http: HttpService
    ) {
    }

    public login(signInValues): Observable<any> {
        return this.http.post('/api/auth/login', signInValues, false);
    }

    public register(signUpValues): Observable<any> {
        return this.http.post('/api/user/', signUpValues, false);
    }

    public changePassword(values): Observable<any> {
        return this.http.post('/api/auth/password', values, true);
    }
}
