import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    public users = [
        {
            img: 'url.com/text.jpg',
            name: 'Chuvak'
        },
        {
            img: 'url.com/text.jpg',
            name: 'Chuvak'
        },
        {
            img: 'url.com/text.jpg',
            name: 'Chuvak'
        },
        {
            img: 'url.com/text.jpg',
            name: 'Chuvak'
        },
    ];

    constructor() { }

    ngOnInit(): void {
    }

}
