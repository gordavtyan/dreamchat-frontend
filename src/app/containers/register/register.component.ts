import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { UserService } from '../../services/user.service';
import { MustMatch } from '../../helpers/must-match';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
    public registerForm: FormGroup;
    public errorMessage;
    public isSubmitted = false;

    private unsubscriber = new Subject<void>();

    constructor(
        private fb: FormBuilder,
        private userService: UserService
    ) { }

    get formControls() {
        return this.registerForm.controls;
    }

    ngOnInit(): void {
        this.registerForm = this.fb.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]],
            passwordRepeat: ['', Validators.required],
        }, {
            validator: MustMatch('password', 'passwordRepeat')
        });
    }


    public register() {
        this.isSubmitted = true;
        this.errorMessage = '';

        if (this.registerForm.invalid) {
            return;
        }

        this.userService.register(this.registerForm.value)
            .pipe(takeUntil(this.unsubscriber))
            .subscribe((data) => {
                console.log(1111111, data);
            });
    }

    ngOnDestroy() {
        this.unsubscriber.next();
        this.unsubscriber.complete();
    }
}
