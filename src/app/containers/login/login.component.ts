import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { HttpService } from 'src/app/services/http.service';
import { AuthService } from 'src/app/services/auth.service';
import { environment } from 'src/environments/environment';
import { UserService } from 'src/app/services/user.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
    public loginForm: FormGroup;
    public errorMessage;
    public isSubmitted = false;
    public brandName = environment.brandName;
    private unsubscriber = new Subject<void>();

    constructor(
        private http: HttpService,
        private router: Router,
        private authService: AuthService,
        private fb: FormBuilder,
        private userService: UserService
    ) {
    }

    public get formControls() {
        return this.loginForm.controls;
    }

    ngOnInit() {
        this.loginForm = this.fb.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    public login() {
        this.isSubmitted = true;
        if (this.loginForm.invalid) {
            return;
        }

        this.userService
            .login(this.loginForm.value)
            .pipe(takeUntil(this.unsubscriber))
            .subscribe((data) => {
                if (data.success) {
                    this.authService.setToken(data.data.token);
                }
                if (!data.success) {
                    this.errorMessage = data.message;
                }
            });
    }

    ngOnDestroy() {
        this.unsubscriber.next();
        this.unsubscriber.complete();
    }
}
